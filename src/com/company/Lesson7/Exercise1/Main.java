package com.company.Lesson7.Exercise1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot();
        RobotCooker robotCooker = new RobotCooker();
        CoffeRobot coffeRobot = new CoffeRobot();
        RobotDancer robotDancer = new RobotDancer();

        robot.work();
        robotCooker.work();
        coffeRobot.work();
        robotDancer.work();

        List <Robot> list = new ArrayList<>();


        list.add(new Robot());
        list.add(new RobotCooker());
        list.add(new RobotDancer());
        list.add(new CoffeRobot());

        for (Robot r :
             list) {
            r.work();
        }
    }
}
