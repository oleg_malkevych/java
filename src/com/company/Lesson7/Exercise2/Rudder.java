package com.company.Lesson7.Exercise2;

public class Rudder {
    private double diameter;
    private String type;
    private String color;


    public void changeColor(){
        this.color = "blue";
    }
    public double getDiameter() {
        return diameter;
    }

    public String getType() {
        return type;
    }

    public String getColor() {
        return color;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Rudder{" +
                "diameter=" + diameter +
                ", type='" + type + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
