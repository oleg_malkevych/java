package com.company.Lesson7.Exercise2;

public class Body {
    private String color;
    private String typeOfBody;
    private int id;


    public void changeId(){
        this.id = 100;
    }

    public String getColor() {
        return color;
    }

    public String getTypeOfBody() {
        return typeOfBody;
    }

    public int getId() {
        return id;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setTypeOfBody(String typeOfBody) {
        this.typeOfBody = typeOfBody;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Body{" +
                "color='" + color + '\'' +
                ", typeOfBody='" + typeOfBody + '\'' +
                ", id=" + id +
                '}';
    }
}
