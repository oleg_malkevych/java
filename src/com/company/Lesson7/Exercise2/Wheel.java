package com.company.Lesson7.Exercise2;

public class Wheel {
    private String type;
    private double diameter;


    public void changeDiameter(){
        this.diameter = 20;
    }

    public String getType() {
        return type;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "type='" + type + '\'' +
                ", diameter=" + diameter +
                '}';
    }
}
