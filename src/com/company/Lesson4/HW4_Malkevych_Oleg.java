package com.company.Lesson4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class HW4_Malkevych_Oleg {
    public static void main(String[] args) {
        Random random = new Random();
        int[] array = new int[20];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100) + 1;
            if (array[i] % 14 == 0)
                System.out.println(array[i]);
        }

    }

}
class HW4_Malkevych_Oleg_2 {
    public static void main(String[] args) {
        int[] array = new int[10];

        initializationArray(array);
        searchNumber(array);

    }


    public static void initializationArray(int[] array){
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
            System.out.print(array[i] + " ");
        }
    }
    public static void searchNumber(int[] array){
        int a;
        List <Integer> list = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            a = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j]){
                    a++;
                    if (a > 1){
                        list.add(array[i]);
                        break;
                    }

                }
            }
        }

        System.out.println("\n" + list.stream().distinct().toList());
    }
}
class HW4_Malkevych_Oleg_3{
    public static void main(String[] args) {
        fibonachiNumber();
    }
    public static void fibonachiNumber(){
        int[] array = new int[10];
        array[0] = 0;
        array[1] = 1;

        for (int i = 2; i < array.length; ++i) {
            array[i] = array[i - 1] + array[i - 2];
        }
        for (int a:
             array) {
            System.out.print(a + " ");
        }
    }
}
class HW4_Malkevych_Oleg_4{
    public static void main(String[] args) {
        Random random = new Random();

        int [] array = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
            System.out.print(array[i] + " ");
        }
        maxNumber(array);
        minNumber(array);
    }
   public static void maxNumber(int[] array){
        int max = 0;
        int indexMax = 0;
       for (int i = 0; i < array.length; i++) {
           if (max < array[i]){
               max = array[i];
               indexMax = i;
           }
       }
       System.out.println("\nIndex max = " + indexMax);
   }
    public static void minNumber(int[] array){
        int min = 10;
        int indexMin = 10;
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]){
                min = array[i];
                indexMin = i;
            }
        }
        System.out.println("Index min = " + indexMin);
    }

}
class HW4_Malkevych_Oleg_5{
    public static void main(String[] args) {
        Random random = new Random();
        int [] a = new int[10];
        for (int i = 0; i < a.length; i++) {
            a[i] = random.nextInt(10);
            System.out.print(a[i] + " ");
        }
        System.out.println();
        int [] b = new int[10];
        for (int i = 0; i < b.length; i++) {
            b[i] = random.nextInt(10);
            System.out.print(b[i] + " ");
        }
        System.out.println();
        int [] c = new int[10];

        for (int i = 0; i < 10; i++) {
            c[i] = a[i] - b[i];
            System.out.print(c[i] + " ");
        }

    }
}
class HW4_Malkevych_Oleg_6 {
    public static void main(String[] args) {
        Random random = new Random();
        int [] a = new int[10];
        for (int i = 0; i < a.length; i++) {
            a[i] = random.nextInt(10);
            System.out.print(a[i] + " ");
        }

        Arrays.sort(a);
        System.out.println("\n" + Arrays.toString(a));

    }
}