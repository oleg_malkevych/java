package com.company.Lesson6;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HW6_Malkevych_Oleh {
    public static void main(String[] args) {
        Random random = new Random();
        List <Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(random.nextInt(20));
        }

        removeOddNumbers(list);

    }
    public static void removeOddNumbers(List<Integer> list) {
        list.removeIf(i -> i % 2 != 0);
        System.out.println(list);
    }
}


